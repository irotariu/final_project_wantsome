FROM python:3

ADD ex2.py /

RUN pip install pystrich

CMD [ "python", "./ex2.py" ]

